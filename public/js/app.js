'use strict';

angular.module('app', ['ngRoute','ngAnimate'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/events', {templateUrl: 'public/views/events.html', controller: 'EventListCtrl'});
        $routeProvider.when('/events/:eventId', {templateUrl: 'public/views/events.html', controller: 'EventCtrl'});
        $routeProvider.when('/events/:eventId/:confirmation', {templateUrl: 'public/views/events.html', controller: 'ConfirmationCrtl'});
        $routeProvider.otherwise({
            redirectTo: '/events'
        });
    }]);

angular.module('app')    
    .controller('MainController', ['$scope', '$rootScope', '$window', '$location', function($scope, $rootScope, $window, $location) {
        $scope.slider = '';     
        $scope.events = events;	
	$rootScope.back = function() {
            $scope.slider = 'slider-right';
            $window.history.back();
        }
        $rootScope.go = function(path) {
            $scope.slider = 'slider-left';
            $location.url(path);
        }
        $rootScope.home = function() {
            $scope.slider = 'slider-left';
            $location.url("/");
        }
    }])
    .controller('EventListCtrl', ['$scope', 'Event', function($scope, Event) {
        console.info('Visited list of Event');
        $scope.view = 'all';
        $scope.events = Event.query();
       
    }])
    .controller('EventCtrl', ['$scope', '$routeParams', 'Event', function($scope, $routeParams, Event) {
        console.info('Visited details of eventId: ' + $routeParams.eventId);
        $scope.view = 'details';
        $scope.event = Event.get($routeParams.eventId);       
    }])
    .controller('ConfirmationCrtl', ['$scope', '$routeParams', 'Event', function($scope, $routeParams, Event) {
        console.info('Visited confirmation of eventId: ' + $routeParams.eventId);
        $scope.view = 'confirmation';
        $scope.event = Event.get($routeParams.eventId);
    }])
    

angular.module('app')
    .factory('Event', [
        function() {
            return {
                query: function() {
                    return events;
                },
                get: function(eventId) { 
                    var eventx = events.find(function(event){
                        return event.id === eventId;
                    });    
                    return eventx;
                }
            }
        }
    ]);