var events = [
    {
        "id" : "1",
        "title" : "Veranstaltung mit vollem Funktionsumfang",	
        "city" : "München",
        "description" : "Das persöniliche Gespräch ist für uns wesentlicher Bestandtell einer guten Geschäftsbeziehung. Regelmäßig laden wit daher zu außergewöhnlichen Veranstaltungen mit informativen Vorträgen ein.",
        "image" : "/event-management/public/images/event.jpg", 
        "date" : "01-05-2016",	
        "status" : "open"
    },
    {
        "id" : "2",
        "title" : "Veranstaltung mit Warteliste",
        "city" : "Regensburg",
        "description" : "Das persöniliche Gespräch ist für uns wesentlicher Bestandtell einer guten Geschäftsbeziehung. Regelmäßig laden wit daher zu außergewöhnlichen Veranstaltungen mit informativen Vorträgen ein.",
        "image" : "/event-management/public/images/event.jpg", 
        "date" : "01-05-2016",
        "status" : "waitlist"
    },
    {
        "id" : "3",
        "title" : "Veranstaltung belegt",
        "city" : "Hamburg",
        "description" : "Das persöniliche Gespräch ist für uns wesentlicher Bestandtell einer guten Geschäftsbeziehung. Regelmäßig laden wit daher zu außergewöhnlichen Veranstaltungen mit informativen Vorträgen ein.",
        "image" : "/event-management/public/images/event.jpg", 
        "date" : "01-04-2016",
        "status" : "closed"
    }
]; 
